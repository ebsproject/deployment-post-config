#!/usr/bin/env bash

USER=ubuntu
PEM_FILE=/path/to/pem_file
DEPLOYMENT_TOOL_PATH=/data/dev/deployment-tool

file=hosts

while read HOST || [ -n "$HOST" ]; do
    echo $HOST
    ssh -i $PEM_FILE $USER@$HOST "export TERM=xterm-256color && cd ${DEPLOYMENT_TOOL_PATH}/utility && bash pull_all_images.sh"
done < "$file"