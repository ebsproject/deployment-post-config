
![image](../../assets/EBS.png)

---
# Bash - Pull EBS Docker Images

### Description

This Bash script pulls all Docker images being used by the Enterprise Breeding System (EBS) in all nodes indicated in the inventory file.

### Prerequisites
- Pem file for SSH access to the hosts that Bash script will be accessing.
  
### Usage

1. Update `hosts` file so that the hosts you want Bash script to execute to are included.
2. Open `run.sh` and update the `USER`, `PEM_FILE`,  and `DEPLOYMENT_TOOL_PATH` variables with the correct values.
3. Run `sh run.sh`.