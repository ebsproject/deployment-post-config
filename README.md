
![image](assets/EBS.png)

---

# Deployment Post Config

> This repository contains all the necessary resources needed for post deployment of EBS.

## General info

### Directory Structure

    |── ansible                 			# Resources for post config using Ansible scripts
    |── assets                   			# Resources such as logos, icons, etc    
    |── bash                                # Resources for post config using Bash scripts