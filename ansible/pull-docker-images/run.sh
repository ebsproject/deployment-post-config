#!/bin/bash

USER=ubuntu
KEY_FILE=/path/to/key_file
INVENTORY_FILE=hosts.yml

export ANSIBLE_HOST_KEY_CHECKING=false && \
    ansible-playbook --private-key $KEY_FILE -u $USER -i $INVENTORY_FILE main.yml