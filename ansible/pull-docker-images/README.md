
![image](../../assets/EBS.png)

---
# Ansible - Pull EBS Docker Images

### Description

This Ansible playbook pulls all Docker images being used by the Enterprise Breeding System (EBS) in all nodes indicated in the inventory file.

### Prerequisites
- Ansible
- Key file for SSH access to the hosts that Ansible will be accessing.
  
### Usage

1. Update `hosts.yml` file so that the hosts you want Ansible to execute to are included.
2. Open `run.sh` and update the `USER` and `KEY_FILE` variables with the correct values.
3. Open `main.yml` and update the `DEPLOYMENT_TOOL_PATH` variable with the correct location of the EBS Deployment Tool in the hosts.
4. Run `sh run.sh`.